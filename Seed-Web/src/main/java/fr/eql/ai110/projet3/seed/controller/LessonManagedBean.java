package fr.eql.ai110.projet3.seed.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.entity.TermTheme;
import fr.eql.ai110.projet3.seed.ibusiness.StudentLevelIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.TermThemeIBusiness;


@ManagedBean(name="mbLesson")
@ViewScoped
public class LessonManagedBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbStudent.student}")
	private Student student;
	private List<TermTheme> themes = new ArrayList<TermTheme>();
	private List<ArrayList<StudentLevel>> levels;
	private ArrayList<StudentLevel> lockedLevels;
	private ArrayList<StudentLevel> openLevels;
	
	@EJB
	private TermThemeIBusiness themeBus;
	@EJB
	private StudentLevelIBusiness levelBus;
	
	@PostConstruct
	public void initialize() {
		themes = themeBus.findAllThemes();
		levels = levelBus.findAllOpenLevels(student);
		
		lockedLevels = levels.get(0);	
		openLevels = levels.get(1);
	}

	
	

	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public List<TermTheme> getThemes() {
		return themes;
	}



	public void setThemes(List<TermTheme> themes) {
		this.themes = themes;
	}



	public TermThemeIBusiness getThemeBus() {
		return themeBus;
	}



	public void setThemeBus(TermThemeIBusiness themeBus) {
		this.themeBus = themeBus;
	}



	public List<ArrayList<StudentLevel>> getLevels() {
		return levels;
	}



	public void setLevels(List<ArrayList<StudentLevel>> levels) {
		this.levels = levels;
	}



	public TermThemeIBusiness getCategoryBus() {
		return themeBus;
	}

	public void setCategoryBus(TermThemeIBusiness categoryBus) {
		this.themeBus = categoryBus;
	}

	public StudentLevelIBusiness getLevelBus() {
		return levelBus;
	}

	public void setLevelBus(StudentLevelIBusiness levelBus) {
		this.levelBus = levelBus;
	}



	public ArrayList<StudentLevel> getLockedLevels() {
		return lockedLevels;
	}



	public void setLockedLevels(ArrayList<StudentLevel> lockedLevels) {
		this.lockedLevels = lockedLevels;
	}



	public ArrayList<StudentLevel> getOpenLevels() {
		return openLevels;
	}



	public void setOpenLevels(ArrayList<StudentLevel> openLevels) {
		this.openLevels = openLevels;
	}
	
	
}
