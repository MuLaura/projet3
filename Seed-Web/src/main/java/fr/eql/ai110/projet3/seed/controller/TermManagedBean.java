package fr.eql.ai110.projet3.seed.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.TermIBusiness;

@ManagedBean(name="mbTerm")
@SessionScoped
public class TermManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Term> termsByCriteria;
	private int index;
	
	@EJB
	private TermIBusiness termBus;
	
	/**
	 * Find all terms whose theme matches the parameter value
	 * @param theme
	 * @return redirection to flashcardpage if result list is not empty
	 */
	public String findTermsByTheme(String theme) {
		String forward = "/findlesson.xhtml?faces-redirection=false";
		
		termsByCriteria = termBus.findAllByTheme(theme);
		
		if (!termsByCriteria.isEmpty()) {
			index = 1;
			forward = "/flashcard.xhtml?faces-redirection=true";
		}
		
		return forward;
	}
	
	/**
	 * Find all terms whose level matches the parameter value
	 * @param theme
	 * @return redirection to flashcardpage if result list is not empty
	 */
	public String findTermsByLevel(int level) {
		String forward = "/findlesson.xhtml?faces-redirection=false";
		
		termsByCriteria = termBus.findAllByLvl(level);
		
		if (!termsByCriteria.isEmpty()) {
			index = 1;
			forward = "/flashcard.xhtml?faces-redirection=true";
		}
		
		return forward;
	}
	
//	public String switchFlashcard(String term) {
//		String forward = "/slashcard.xhtml?faces-redirection=false";
//		
//		return forward;
//	}

	public List<Term> getTermsByCriteria() {
		return termsByCriteria;
	}

	public void setTermsByCriteria(List<Term> termsByCriteria) {
		this.termsByCriteria = termsByCriteria;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	
}
