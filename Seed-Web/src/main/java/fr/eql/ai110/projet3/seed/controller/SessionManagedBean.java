package fr.eql.ai110.projet3.seed.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.ExerciseIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.LearningIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.StudentIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.TermIBusiness;

@ManagedBean(name="mbSession")
@SessionScoped
public class SessionManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{mbStudent.student}")
	private Student student;
	private List<Learning> lessonSessionContents;
	private List<Learning> learningsForExerciseSession;
	private List<Exercise> exerciseSessionContents;
	private List<Term> sessionTerms;
	private Term currentTerm;
	private Exercise currentExercise;
	private String currentAnswer;
	private Boolean answerIsRight = true;
	private String acqLvl1 = "Novice";

	@EJB
	private LearningIBusiness learningBus;

	@EJB
	private ExerciseIBusiness exBus;

	@EJB
	private TermIBusiness termBus;
	
	@EJB
	private StudentIBusiness studBus;

	/**
	 * Launch a new lesson session if content is available
	 * @return redirect to lesson-session page
	 */
	public String startLessonSession() {
		String forward = "/index.xhtml?faces-redirection=false";
		lessonSessionContents = learningBus.findLessonSessionContent(student);

		if (lessonSessionContents.size() > 0) {
			forward= "/lesson-session.xhtml?faces-redirection=true";
		}

		return forward;
	}

	/**
	 * End lesson session
	 * @return redirection to index page
	 */
	public String endLessonSession() {
		String forward = "";

		if (learningBus.updateAfterSession(lessonSessionContents)) {
			forward= "/index.xhtml?faces-redirection=true";
		}

		return forward;
	}

	/**
	 * Launch an exercise session if contents are available
	 * @return exercise-session pages or not-found page if empty
	 */
	public String startExerciseSession() {
		String forward = "/index.xhtml?faces-redirection=false";

		exerciseSessionContents = exBus.findExercisesForSessionContent(student);
		learningsForExerciseSession = learningBus.findLearningForNewExercises(student);
		sessionTerms = termBus.getTermsForSessionContent(student);

		if (!exerciseSessionContents.isEmpty()) {

			String exerciseType = exerciseSessionContents.get(0).getExercise_type().getExerciseType();
			currentExercise = exerciseSessionContents.get(0);
			currentTerm = currentExercise.getTerm();

			if (exerciseType.compareTo("Compréhension Orale") == 0) {
				forward= "/exercise-session-co.xhtml?faces-redirection=true";
			} else {
				forward= "/exercise-session.xhtml?faces-redirection=true";
			} 

		} else {
			forward = "/error.xhtml?faces-redirection=true";
		}

		return forward;
	}

	/**
	 * Verify student answer against expected answer and update Exercise and Learning values accordingly
	 * @return next exercise page if correct
	 * @return no redirection if false
	 * @return index page if last exercise in list
	 */
	public String checkAnswer() {
		String forward = "";
		currentTerm = currentExercise.getTerm();

		if (currentAnswer.trim().compareTo(currentExercise.getExpectedAnswer().trim()) == 0) {
			answerIsRight = true;

			updateCurrentExerciseValuesWhenCorrect();
			currentExercise = exBus.updateExerciseStats(currentExercise);

			if(currentExercise.getExerciseId() != null) {

				if (exerciseSessionContents.size() == 1) {
					learningBus.updateAfterExerciseSession(student, learningsForExerciseSession);
					exerciseSessionContents.clear();
					
					checkProgressAndLevelUpIfComplete();
					
					currentAnswer = "";
					forward = "/index.xhtml?faces-redirection=true";

				} else {
					exerciseSessionContents.remove(0);
					currentExercise = exerciseSessionContents.get(0);
					currentTerm = currentExercise.getTerm();
					String currentType = currentExercise.getExercise_type().getExerciseType();

					if (currentType.trim().compareToIgnoreCase("Compréhension Orale") == 0) {
						currentAnswer = "";
						forward= "/exercise-session-co.xhtml?faces-redirection=true";

					} else {
						currentAnswer = "";
						forward= "/exercise-session.xhtml?faces-redirection=true";
					} 
				}
			}

		} else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Réponse fausse", 
					"La réponse saisie est incorrecte. Vérifie l'orthographe et réessaie !");
			FacesContext.getCurrentInstance().addMessage("answerForm:answer", facesMessage);

			answerIsRight = false;
			updateCurrentExerciseValuesWhenIncorrect();
			currentExercise = exBus.updateExerciseStats(currentExercise);

			for (Learning learning : learningsForExerciseSession) {
				if (currentTerm.getEnglishTerm().trim().equalsIgnoreCase(learning.getTerm().getEnglishTerm().trim())) {
					learning.setRevisionStreak(0);
				}
			}
			currentAnswer = "";
			forward = null;
		}
		return forward;
	}

	private void checkProgressAndLevelUpIfComplete() {
		int totalTerms = termBus.findNbTermByLvl(student.getStudentlevel().getIntLevel());
		int acquiredTerms = learningBus.findNbForAllButOneAcqLvlByAcqLvlAndStudentLvl(acqLvl1, student);
		int progress = Math.round(100 * acquiredTerms / totalTerms);
		
		if (progress == 100) {
			student = studBus.levelUp(student);
		}
		
	}

	/**
	 * update Exercise values when student answer is correct
	 */
	private void updateCurrentExerciseValuesWhenCorrect() {

		if(currentExercise.getExerciseStreak() == null) {
			currentExercise.setExerciseStreak(1);
		} else {
			currentExercise.setExerciseStreak(currentExercise.getExerciseStreak() +1);
		}

		if (currentExercise.getNbCorrect() == null) {
			currentExercise.setNbCorrect(1);
		} else {
			currentExercise.setNbCorrect(currentExercise.getNbCorrect() + 1);
		}

		if (currentExercise.getTotalAnswers() == null) {
			currentExercise.setTotalAnswers(1);
		} else {
			currentExercise.setTotalAnswers(currentExercise.getTotalAnswers() + 1);
		}

		if(currentExercise.getTerm() != currentTerm) {
			currentExercise.setTerm(currentTerm);
		}

		if(currentExercise.getStudent() == null) {
			currentExercise.setStudent(student);
		}
	}

	/**
	 * update Exercise values when student answer is incorrect
	 */
	private void updateCurrentExerciseValuesWhenIncorrect() {
		currentExercise.setExerciseStreak(0);

		if (currentExercise.getNbIncorrect() == null) {
			currentExercise.setNbIncorrect(1);
		} else {
			currentExercise.setNbIncorrect(currentExercise.getNbIncorrect() + 1);
		}

		if (currentExercise.getTotalAnswers() == null) {
			currentExercise.setTotalAnswers(1);
		} else {
			currentExercise.setTotalAnswers(currentExercise.getTotalAnswers() + 1);
		}

		if(currentExercise.getTerm() != currentTerm) {
			currentExercise.setTerm(currentTerm);
		}

		if(currentExercise.getStudent() == null) {
			currentExercise.setStudent(student);
		}
	}

	public List<Learning> getLessonSessionContents() {
		return lessonSessionContents;
	}

	public void setLessonSessionContents(List<Learning> lessonSessionContents) {
		this.lessonSessionContents = lessonSessionContents;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public List<Exercise> getExerciseSessionContents() {
		return exerciseSessionContents;
	}

	public void setExerciseSessionContents(List<Exercise> exerciseSessionContents) {
		this.exerciseSessionContents = exerciseSessionContents;
	}

	public List<Term> getSessionTerms() {
		return sessionTerms;
	}

	public void setSessionTerms(List<Term> sessionTerms) {
		this.sessionTerms = sessionTerms;
	}

	public Term getCurrentTerm() {
		return currentTerm;
	}

	public void setCurrentTerm(Term currentTerm) {
		this.currentTerm = currentTerm;
	}

	public Exercise getCurrentExercise() {
		return currentExercise;
	}

	public void setCurrentExercise(Exercise currentExercise) {
		this.currentExercise = currentExercise;
	}

	public String getCurrentAnswer() {
		return currentAnswer;
	}

	public void setCurrentAnswer(String currentAnswer) {
		this.currentAnswer = currentAnswer;
	}

	public Boolean getAnswerIsRight() {
		return answerIsRight;
	}

	public void setAnswerIsRight(Boolean answerIsRight) {
		this.answerIsRight = answerIsRight;
	}

}
