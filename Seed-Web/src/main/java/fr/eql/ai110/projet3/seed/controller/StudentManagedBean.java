package fr.eql.ai110.projet3.seed.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Size;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.ibusiness.StudentIBusiness;

@ManagedBean(name="mbStudent")
@SessionScoped
public class StudentManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Logger log = LogManager.getLogger();
	
	@Size(min=3, max=15)
	private String username;
	@Size(min=5)
	private String password;
	private String email;
	private Boolean acceptsTerms;
	private Student student;
	private Boolean isLoggedIn;
	
	@EJB
	private StudentIBusiness studentBus;
	
	/**
	 * Create new student with user input values
	 * @return redirection to index page if process completes correctly
	 */
	public String register() {
		String forward = null;
			if (acceptsTerms == false) {
				return forward;
			}
			
			if (studentBus.verifyIfExists(username, email)) {
				log.warn("Username or email already exists. New user cannot be created.");
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Identifiant ou e-mail déjà pris.", 
						"Un utilisateur avec cet email/identifiant existe déjà.");
				FacesContext.getCurrentInstance().addMessage("signup", facesMessage);
				return forward;
			} else {
				student = studentBus.createNewUser(username, password, email);
				log.info("User created : session started");
				forward = "/index.xhtml?faces-redirection=true";
			}
			
		return forward;
	}

	/**
	 * Student login if input matches database values
	 * @return redirection to index page if process completes correctly
	 */
	public String connect() {
		String forward = null;
		
		student = studentBus.connect(username, password);
		
		if (isConnected()) {
			log.info("Session open : user connected");
			forward = "/index.xhtml?faces-redirection=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login/Password incorrect", 
					"Incorrect login/password");
			FacesContext.getCurrentInstance().addMessage("loginForm:inLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inPassword", facesMessage);
			
			forward= "/login.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	/**
	 * Determines whether there is a student in session
	 * @return true if student is logged in
	 */
	public boolean isConnected() {
	
		if(student!=null) {
			isLoggedIn = true;
		} else {
			isLoggedIn = false;
		}
		
		return isLoggedIn;
	}
	
	/**
	 * End session and disconnect user
	 * @return redirection to landingpage if process completes correctly
	 */
	public String disconnect() {
		
		if(isConnected()) {
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.invalidate();
			student = null;
			log.info("Session close : user disconnected");
			return "/landingpage.xhtml?faces-redirection=true";
		}
		return null;
	}
	
	/**
	 * Save updated student if user input does not match an existing user
	 * @return profile page
	 */
	public String save() {
		String forward = null;
		
		if (studentBus.verifyIfExists(student.getUsername(), student.getPassword())) {
			student = studentBus.findStudentByID(student.getUserId());
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Identifiant ou e-mail déjà pris.", 
					"Un utilisateur avec cet email/identifiant existe déjà.");
			FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
			forward = "/profile.xhtml?faces-redirection=true";
			return forward;
		} else {
			studentBus.save(student);
			forward = "/profile.xhtml?faces-redirection=true";
			return forward;
		}
	}
	
	/**
	 * blocks unautherized access to connected pages and redirects to login page
	 * @throws IOException
	 */
	public void redirectToLoginIfNotLoggedIn() throws IOException {
        if (!isConnected()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/login.xhtml");
        }
    }

	
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public StudentIBusiness getStudentBus() {
		return studentBus;
	}

	public void setStudentBus(StudentIBusiness studentBus) {
		this.studentBus = studentBus;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Boolean getIsLoggedIn() {
		return isLoggedIn;
	}

	public void setIsLoggedIn(Boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	public Boolean getAcceptsTerms() {
		return acceptsTerms;
	}

	public void setAcceptsTerms(Boolean acceptsTerms) {
		this.acceptsTerms = acceptsTerms;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
