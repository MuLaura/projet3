package fr.eql.ai110.projet3.seed.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.ibusiness.LearningIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.StudentIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.TermIBusiness;

@ManagedBean(name="mbLearning")
@ViewScoped
public class LearningManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbStudent.student}")
	private Student student;
	private int progress;
	private List<Learning> todayUpcomingReviews;
	private List<Learning> tomorrowUpcomingReviews;
	private List<Learning> afterTomorrowUpcomingReviews;
	private ArrayList<Integer> timesToday;
	private ArrayList<Integer> timesTomorrow;
	private ArrayList<Integer> timesAfterTomorrow;
	private String acqLvl1 = "Novice";
	private String acqLvl2 = "Apprenti";
	private String acqLvl3 = "Étudiant";
	private String acqLvl4 = "Maître";
	private String acqLvl5 = "Grand Sage";
	private Boolean areNoExercises;
	private Boolean areNoLessons;
	
	@EJB
	private LearningIBusiness learningBus;
	@EJB
	private TermIBusiness termBus;
	@EJB
	private StudentIBusiness studBus;
	
	@PostConstruct
	public void initialize() {
		getReviewCalendar();
		
	}
	
	/**
	 * Show by hour the number of exercises that will become available over the next three days
	 */
	public void getReviewCalendar() {
		LocalDate reviewDate = LocalDate.now(ZoneOffset.ofHours(0));		
		
		todayUpcomingReviews = learningBus.findUpcomingReviews(reviewDate, student);
		timesToday = new ArrayList<Integer>();
		addReviewTimes(reviewDate, timesToday, todayUpcomingReviews);
		
		LocalDate tomorrowReviewDate = reviewDate.plusDays(1);
		tomorrowUpcomingReviews = learningBus.findUpcomingReviews(tomorrowReviewDate, student);
		timesTomorrow = new ArrayList<Integer>();
		addReviewTimes(tomorrowReviewDate, timesTomorrow, tomorrowUpcomingReviews);
		
		LocalDate afterTomorrowReviewDate = tomorrowReviewDate.plusDays(1);
		afterTomorrowUpcomingReviews = learningBus.findUpcomingReviews(afterTomorrowReviewDate, student);
		timesAfterTomorrow = new ArrayList<Integer>();
		addReviewTimes(afterTomorrowReviewDate, timesAfterTomorrow, afterTomorrowUpcomingReviews);
	}
	
	/**
	 * Find the number of reviews available for a given time at a given date
	 * @param reviewDate
	 * @param times
	 * @param upcomingReviews
	 */
	private void addReviewTimes(LocalDate reviewDate, ArrayList<Integer> times, List<Learning> upcomingReviews) {
		LocalDateTime reviewDateTime = reviewDate.atStartOfDay();
			for (int i = 0; i < 24; i++) {
				int nbReviewsPerInstant = 0;
				for (Learning learning : upcomingReviews) {	
					if (reviewDateTime.isEqual(learning.getNextReviewDate().truncatedTo(ChronoUnit.HOURS))) {
						nbReviewsPerInstant++;
					}
				}
				times.add(i, nbReviewsPerInstant);
				reviewDateTime = reviewDateTime.plusHours(1);
			}
	}
	
	/**
	 * Calculate student progression by getting the percentage of terms above Novice acquistion Level. If complete, level up.
	 * @return string value of progression percentage
	 */
	public String calculateProgress() {
		int totalTerms = termBus.findNbTermByLvl(student.getStudentlevel().getIntLevel());
		int acquiredTerms = learningBus.findNbForAllButOneAcqLvlByAcqLvlAndStudentLvl(acqLvl1, student);
		progress = Math.round(100 * acquiredTerms / totalTerms);
		
		if (progress == 100) {
//			student = studBus.levelUp(student);
			totalTerms = termBus.findNbTermByLvl(student.getStudentlevel().getIntLevel());
			acquiredTerms = learningBus.findNbByAcqLvlAndStudentLvl(acqLvl1, student);
			progress = Math.round(100 * acquiredTerms / totalTerms);
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Félicitations !", 
					"Tu passes au niveau suivant :)");
			FacesContext.getCurrentInstance().addMessage("growlMsg", facesMessage);
		}
		
		return String.valueOf(progress);
	}
	
	/**
	 * Show the number of terms in a level according to their acquisitionLevel and the studentID
	 * @param acquisitionLvl
	 * @return int number of terms in level
	 */
	public int progressOverview(String acquisitionLvl) {
		return learningBus.findNbByAcqLvlAndStudentLvl(acquisitionLvl, student);
	}
	
	/**
	 * Change appearance of progress bar according to progress value
	 * @return string appearance
	 */
	public String setProgressBarLook() {
		String look = new String();
		
		if (progress <= 20) {
			look = "danger";
		} else if (progress > 20 && progress <= 40) {
			look = "warning";
		} else if (progress > 40 && progress <= 60) {
			look = "info";
		} else if (progress > 60 && progress <= 80) {
			look = "primary";
		} else if (progress > 80 && progress <= 100) {
			look = "success";
		}
		return look;
	}
	
	/**
	 * Add up the number of reviews that will become available by hour 
	 * to display an overview of the total reviews available at a given moment
	 * @param reviewIndex
	 * @param times
	 * @return total reviews available at a given Datetime
	 */
	public int addingUpReview(int reviewIndex, ArrayList<Integer> times) {
		int total = 0;
		
		for (int i = 0; i <= reviewIndex; i++) {
			total += times.get(i);
		}
		
		return total;
	}
	
	/**
	 * Regulate display of review forecast based on the value
	 * @param reviewIndex
	 * @param times
	 * @return style display value
	 */
	public String defineDisplay(int reviewIndex, ArrayList<Integer> times) {
		String display = "none";
		if (times.get(reviewIndex) != 0) {
			display = "flex";
		}
		return display;
	}

	/**
	 * Change icon based on totalReviews value in Review Forecast
	 * @param totalReviews
	 * @return
	 */
	public String showWeatherIcon(int totalReviews) {
		String icon = new String();
		
		if (totalReviews <= 10) {
			icon = "fa-sun-o";
		} else if (totalReviews > 10 && totalReviews <= 20) {
			icon = "fa-cloud";
		} else if (totalReviews > 20 && totalReviews <= 30) {
			icon = "fa-tint";
		} else if (totalReviews > 30) {
			icon = "fa-bolt";
		}
		return icon;
	}
	
	/**
	 * Find the number of all new lessons available to a connected user 
	 * @return number of available lessons
	 */
	public int findNbNewLessons() {
		
		int nbNewLessons = learningBus.findNbNewLessons(student);
		
		if(nbNewLessons == 0) {
			areNoLessons = true;
		} else {
			areNoLessons = false;
		}
		return nbNewLessons;
	}
	
	/**
	 * Find the number of all new exercises available to a connected user 
	 * @return number of available exercises
	 */
	public int findNbNewExercises() {
		int nbNewExercises = learningBus.findNbNewExercises(student);
		
		if(nbNewExercises == 0) {
			areNoExercises = true;
		} else { 
			areNoExercises = false;
		}
		
		return nbNewExercises;
	}
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public List<Learning> getTodayUpcomingReviews() {
		return todayUpcomingReviews;
	}

	public void setTodayUpcomingReviews(List<Learning> todayUpcomingReviews) {
		this.todayUpcomingReviews = todayUpcomingReviews;
	}

	public List<Learning> getTomorrowUpcomingReviews() {
		return tomorrowUpcomingReviews;
	}

	public void setTomorrowUpcomingReviews(List<Learning> tomorrowUpcomingReviews) {
		this.tomorrowUpcomingReviews = tomorrowUpcomingReviews;
	}

	public List<Learning> getAfterTomorrowUpcomingReviews() {
		return afterTomorrowUpcomingReviews;
	}

	public void setAfterTomorrowUpcomingReviews(List<Learning> afterTomorrowUpcomingReviews) {
		this.afterTomorrowUpcomingReviews = afterTomorrowUpcomingReviews;
	}

	public ArrayList<Integer> getTimesToday() {
		return timesToday;
	}

	public void setTimesToday(ArrayList<Integer> timesToday) {
		this.timesToday = timesToday;
	}

	public ArrayList<Integer> getTimesTomorrow() {
		return timesTomorrow;
	}

	public void setTimesTomorrow(ArrayList<Integer> timesTomorrow) {
		this.timesTomorrow = timesTomorrow;
	}

	public ArrayList<Integer> getTimesAfterTomorrow() {
		return timesAfterTomorrow;
	}

	public void setTimesAfterTomorrow(ArrayList<Integer> timesAfterTomorrow) {
		this.timesAfterTomorrow = timesAfterTomorrow;
	}

	public String getAcqLvl1() {
		return acqLvl1;
	}

	public void setAcqLvl1(String acqLvl1) {
		this.acqLvl1 = acqLvl1;
	}

	public String getAcqLvl2() {
		return acqLvl2;
	}

	public void setAcqLvl2(String acqLvl2) {
		this.acqLvl2 = acqLvl2;
	}

	public String getAcqLvl3() {
		return acqLvl3;
	}

	public void setAcqLvl3(String acqLvl3) {
		this.acqLvl3 = acqLvl3;
	}

	public String getAcqLvl4() {
		return acqLvl4;
	}

	public void setAcqLvl4(String acqLvl4) {
		this.acqLvl4 = acqLvl4;
	}

	public String getAcqLvl5() {
		return acqLvl5;
	}

	public void setAcqLvl5(String acqLvl5) {
		this.acqLvl5 = acqLvl5;
	}

	public Boolean getAreNoExercises() {
		return areNoExercises;
	}

	public void setAreNoExercises(Boolean areNoExercises) {
		this.areNoExercises = areNoExercises;
	}

	public Boolean getAreNoLessons() {
		return areNoLessons;
	}

	public void setAreNoLessons(Boolean areNoLessons) {
		this.areNoLessons = areNoLessons;
	}	
	
}
