package fr.eql.ai110.projet3.seed.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.ibusiness.StudentLevelIBusiness;
import fr.eql.ai110.projet3.seed.idao.StudentIDAO;
import fr.eql.ai110.projet3.seed.idao.StudentLevelIDAO;

@Remote(StudentLevelIBusiness.class)
@Stateless
public class StudentLevelBusiness implements StudentLevelIBusiness{
	
	@EJB
	private StudentLevelIDAO dao;
	@EJB
	private StudentIDAO studentDAO;
	
	@Override
	public List<ArrayList<StudentLevel>> findAllOpenLevels(Student student) {
		
		List<StudentLevel> allLevels = dao.getAll();
		ArrayList<StudentLevel> openLevels = new ArrayList<StudentLevel>();
		ArrayList<StudentLevel> lockedLevels = new ArrayList<StudentLevel>();
		List<ArrayList<StudentLevel>> listOfLists = new ArrayList<ArrayList<StudentLevel>>();
		
		for (StudentLevel level : allLevels) {
			if (level.getIntLevel() > student.getStudentlevel().getIntLevel()) {
				lockedLevels.add(level);
			} else {
				openLevels.add(level);
			}
		}
		listOfLists.add(lockedLevels);
		listOfLists.add(openLevels); 
		return listOfLists; 
	}

}
