package fr.eql.ai110.projet3.seed.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.eql.ai110.projet3.seed.entity.AcquisitionLevel;
import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.StudentIBusiness;
import fr.eql.ai110.projet3.seed.idao.AcquisitionLevelIDAO;
import fr.eql.ai110.projet3.seed.idao.LearningIDAO;
import fr.eql.ai110.projet3.seed.idao.StudentIDAO;
import fr.eql.ai110.projet3.seed.idao.StudentLevelIDAO;
import fr.eql.ai110.projet3.seed.idao.TermIDAO;

@Remote(StudentIBusiness.class)
@Stateless
public class StudentBusiness implements StudentIBusiness {

	private static final int MIN_USERNAME_LENGTH = 3;

	private static final int MAX_USERNAME_LENGTH = 15;

	private Logger log = LogManager.getLogger();
	
	@EJB
	private StudentIDAO studentDAO;
	
	@EJB
	private StudentLevelIDAO lvlDAO;
	
	@EJB
	private LearningIDAO learnDAO;
	
	@EJB
	private TermIDAO termDAO;
	
	@EJB
	private AcquisitionLevelIDAO acqDAO;
	
	@Override
	public Student findStudentByID(int id) {
		Student student = studentDAO.getById(id);
		return student;
	}

	@Override
	public Student connect(String username, String password) {
		Student student = studentDAO.getStudentByUsername(username);
		StringBuffer myBuffer = new StringBuffer(password);
		myBuffer.insert(3, student.getSalt());
		password = myBuffer.toString();
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			password = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			log.error("Error hashing password: " + e);
			e.printStackTrace();
		}
			
		return studentDAO.authenticate(username, password);
	}

	@Override
	public Student levelUp(Student student) {
		int currentLvl = student.getStudentlevel().getIntLevel();
		List<StudentLevel> allLevels = lvlDAO.getAll();
		StudentLevel maxLvl = allLevels.get(allLevels.size() -1);
		Student updatedStudent = student;
		
		if (currentLvl != maxLvl.getIntLevel()) {
			int nextLvl = currentLvl + 1;
		
			for (StudentLevel level : allLevels) {
				if(level.getIntLevel() == nextLvl) {
					student.setStudentlevel(level);
				}
			}
			
			student = unlockNewTermsByLvl(student);
			updatedStudent = studentDAO.update(student);
		} else {
			log.info("Student has already reached max level.");
		}
		
		return updatedStudent;
	}

	/**
	 * Adds new learning objects to table for all previously locked terms in a level for a given user 
	 * @param student
	 * @return student with updated list of learning objects
	 */
	public Student unlockNewTermsByLvl(Student student) {
		List<Term> nextLevelTerms = termDAO.getAllByLevel(student.getStudentlevel().getIntLevel());
		AcquisitionLevel novice = acqDAO.getByName("Novice");
		
		for (Term term : nextLevelTerms) {
			LocalDateTime now = LocalDateTime.now();
			Learning learning = new Learning(null, now, null, null, null, null, null, student, novice, term);
			student.getLearnings().add(learning);
		}
		
		return student;
	}

	@Override
	public boolean verifyIfExists(String username, String email) {
		Boolean isExistingUser = false;
		Student student = studentDAO.getStudentByUsername(username);
		Student student2 = studentDAO.getStudentByEmail(email);
		
		if (student == null && student2 == null) {
			isExistingUser = false;
		} else {
			isExistingUser = true;
			log.warn("Username or email already taken: new user was not created.");
		}
		
		return isExistingUser;
	}

	@Override
	public Student createNewUser(String username, String password, String email) {
		LocalDate today = LocalDate.now();
		
		StudentLevel beginnerLvl = lvlDAO.getByIntLvl(1);
		int defaultNbLessonsPerSession = 5;
		if (validateUsernameConstraints(username)) {
			Student newStudent = new Student(null, username, password, null, email, today, null, today, false, defaultNbLessonsPerSession, null, null, beginnerLvl);
		
		
		LocalDateTime todayDateTime = LocalDateTime.now();
		AcquisitionLevel novice = acqDAO.getByName("Novice");
		List<Term> lvl1Terms = termDAO.getAllByLevel(beginnerLvl.getIntLevel());
		List<Learning> studentLearning = new ArrayList<Learning>();
		for (Term term : lvl1Terms) {
			Learning learning = new Learning(null, todayDateTime, null, null, null, null, null, newStudent, novice, term);
			studentLearning.add(learning);
		}
		
		newStudent.setLearnings(studentLearning);
		
		String salt = generateSalt();
		newStudent.setSalt(salt);
		
		String hashedPassword = hashPassword(salt, password);
		newStudent.setPassword(hashedPassword);
		
		newStudent = studentDAO.add(newStudent);
		return newStudent;
		} else {
			return null;
		}
		
	}
	
	public boolean validateUsernameConstraints(String username) {
		Boolean isValid = false;
		if (username.length() >= MIN_USERNAME_LENGTH && username.length() <= MAX_USERNAME_LENGTH) {
			String regEx = "^[-\\w.]+$"; 
			
			Pattern p = Pattern.compile(regEx);
	        
	        Matcher m = p.matcher(username);
	        if(!m.matches()) {
	            isValid = false;
	        } else {
	        	isValid = true;
	        }
		}
		return isValid;
	}

	private String generateSalt() {
		Random aRandom = new Random();
		int myRandom = aRandom.nextInt(999);
		String salt = Integer.toString(myRandom);
		return salt;
}

	public String hashPassword(String salt, String password) {
		StringBuffer myBuffer = new StringBuffer(password);
		myBuffer.insert(3, salt);
		String saltedPassword = myBuffer.toString();
		
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(saltedPassword.getBytes(StandardCharsets.UTF_8));
			String hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
			saltedPassword = hashedPassword;
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			log.error("Password could not be hashed" + e);
		}
		return saltedPassword;
	}

	@Override
	public void save(Student student) {
		studentDAO.update(student);		
	}
	
}
