package fr.eql.ai110.projet3.seed.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.ExerciseType;
import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.TermIBusiness;
import fr.eql.ai110.projet3.seed.idao.ExerciseIDAO;
import fr.eql.ai110.projet3.seed.idao.ExerciseTypeIDAO;
import fr.eql.ai110.projet3.seed.idao.LearningIDAO;
import fr.eql.ai110.projet3.seed.idao.TermIDAO;

@Remote(TermIBusiness.class)
@Stateless
public class TermBusiness implements TermIBusiness {

	@EJB
	private TermIDAO dao;
	
	@EJB
	private LearningIDAO learnDAO;

	@Override
	public List<Term> findAllByLvl(int lvl) {
		return dao.getAllByLevel(lvl);
	}

	@Override
	public List<Term> findAllByTheme(String theme) {
		return dao.getAllByTheme(theme);
	}

	@Override
	public int findNbTermByLvl(int lvl) {
		Long nbTerms = dao.findNbTermsByLevel(lvl);
		int termNb;
		
		if (nbTerms == null) {
			nbTerms = dao.findNbTermsByLevel(1);
		} 
			termNb = nbTerms.intValue();
		
		return termNb;
	}
	
	@Override
	public List<Term> getTermsForSessionContent(Student student) {
		
		List<Learning> exercisesUnlockedAtCurrentDate = learnDAO.getAvailableExercises(student);
		List<Term> sessionTerms = new ArrayList<Term>();
		
		for (Learning learning : exercisesUnlockedAtCurrentDate) {
			Term term = dao.getTermByID(learning.getTerm().getTermId());
			sessionTerms.add(term);
		}
		 
		return sessionTerms;
	}
	
}
