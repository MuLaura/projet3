package fr.eql.ai110.projet3.seed.business;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.eql.ai110.projet3.seed.entity.AcquisitionLevel;
import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.LearningIBusiness;
import fr.eql.ai110.projet3.seed.idao.AcquisitionLevelIDAO;
import fr.eql.ai110.projet3.seed.idao.LearningIDAO;
@Remote(LearningIBusiness.class)
@Stateless
public class LearningBusiness implements LearningIBusiness{
	
	private Logger log = LogManager.getLogger();
	
	@EJB
	private LearningIDAO dao;
	
	@EJB
	private AcquisitionLevelIDAO acqDAO;
	
	@Override
	public int findNbByAcqLvlAndStudentLvl(String acqLvl, Student student) {
		Long acqLvlNb = dao.getNbTermByAcqLvlAndTermLvl(acqLvl, student);
		
		int nbAcqLvl;
		
		if (acqLvlNb == null) {
			nbAcqLvl = 0;
		} else {
			nbAcqLvl = acqLvlNb.intValue();
		}
		
		return nbAcqLvl;
	}

	@Override
	public List<Learning> findUpcomingReviews(LocalDate reviewDate, Student student) {
		 
		return dao.getUpcomingReviews(reviewDate, student);
	}

	@Override
	public int findNbNewLessons(Student student) {
		List<Learning> availableLessons = dao.getNewLessons(student);
		return availableLessons.size();
	}

	@Override
	public int findNbNewExercises(Student student) {
		List<Learning> availableExercises = dao.getAvailableExercises(student);
		return availableExercises.size();
	}
	@Override
	public List findLearningForNewExercises(Student student) {
		return dao.getAvailableExercises(student);
	}

	@Override
	public List<Learning> findLessonSessionContent(Student student) {
	List<Learning> lessons = dao.getNewLessons(student);
	
	if (lessons.size() > student.getNbLessonsPerClass()) {
		List<Learning> sessionContents = new ArrayList(lessons.subList(0, student.getNbLessonsPerClass()));
		lessons.clear();
		lessons = sessionContents;
	} 	
		return lessons;
	}

	@Override
	public Boolean updateAfterSession(List<Learning> lessonContents) {
		Boolean isUpdated = false;
		
		for (Learning lesson : lessonContents) {
			LocalDateTime today = LocalDateTime.now();
			lesson.setLastViewDate(today);
			lesson.setNextReviewDate(today);
			
			Learning updatedLesson = dao.update(lesson);
			
			if (updatedLesson.getNextReviewDate() == today) {
				isUpdated = true;
			} else {
				isUpdated = false;
				log.warn("Lesson was not be updated in the database");
			}
		}
		return isUpdated;
	}

	@Override
	public Boolean updateAfterExerciseSession(Student student, List<Learning> completedExercises) {
		Boolean isUpdated = false;
		
		for (Learning learning : completedExercises) {
			LocalDateTime today = LocalDateTime.now();
			learning.setLastViewDate(today);
			
			if (learning.getRevisionStreak() == null || learning.getRevisionStreak() != 0) {
				
				updateLearningValuesWhenAllAnswersCorrect(learning);
				
			} else {
				updateLearningValuesWhenSomeMistakes(learning);
			}
			
			long cooldownPeriod = learning.getAcquisitionlvl().getAcqLvlIntervalInHours();
			learning.setNextReviewDate(today.plusHours(cooldownPeriod));
			
			
			if (learning.getTotalViews() == null) {
				learning.setTotalViews(1);
			} else {
				learning.setTotalViews(learning.getTotalViews()+1);
			}
			
			Learning updatedLearning = dao.update(learning);
			
			if (updatedLearning != null) {
				isUpdated = true;
			}
		}
		
		return isUpdated;
	}

	/**
	 * Set Learning values after exercise session when user made no mistakes on this term's exercises
	 * @param learning
	 * @return updated Learning object
	 */
	private Learning updateLearningValuesWhenAllAnswersCorrect (Learning learning) {
		
		if(learning.getRevisionStreak() == null) {
			learning.setRevisionStreak(1);
		} else {
			learning.setRevisionStreak(learning.getRevisionStreak()+1);
		}
		
		if (learning.getTotalCorrect() == null) {
			learning.setRevisionStreak(1);
		} else {
			learning.setTotalCorrect(learning.getTotalCorrect()+1);
		}
		
		if(learning.getAcquisitionlvl() == null) {
			
			AcquisitionLevel newLevel = acqDAO.getById(1);
			
			learning.setAcquisitionlvl(newLevel);
			
		} else {
			AcquisitionLevel newLevel = acqDAO.getById(learning.getAcquisitionlvl().getAcquisitionlvlId() + 1);
			learning.setAcquisitionlvl(newLevel);
		}
		
		return learning;
	}
	
	/**
	 * Set Learning values after exercise session when user made some mistakes on this term's exercises
	 * @param learning
	 * @return updated Learning object
	 */
	private Learning updateLearningValuesWhenSomeMistakes (Learning learning) {
		if(learning.getAcquisitionlvl() == null) {
			
			AcquisitionLevel newLevel = acqDAO.getById(1);
			learning.setAcquisitionlvl(newLevel);
			
		} else if(!learning.getAcquisitionlvl().getLvlName().trim().equalsIgnoreCase("Novice")) {
			
			AcquisitionLevel newLevel = acqDAO.getById(learning.getAcquisitionlvl().getAcquisitionlvlId() - 1);
			learning.setAcquisitionlvl(newLevel);
		}
		
		return learning;
	}

	@Override
	public int findNbForAllButOneAcqLvlByAcqLvlAndStudentLvl(String acqLvl, Student student) {
		Long acqLvlNb = dao.getNbTermByAcqLvlAndTermLvlForAllButOne(acqLvl, student);
		
		int nbAcqLvl;
		
		if (acqLvlNb == null) {
			nbAcqLvl = 0;
		} else {
			nbAcqLvl = acqLvlNb.intValue();
		}
		
		return nbAcqLvl;
	}
	
	

}
