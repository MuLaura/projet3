package fr.eql.ai110.projet3.seed.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.TermTheme;
import fr.eql.ai110.projet3.seed.ibusiness.TermThemeIBusiness;
import fr.eql.ai110.projet3.seed.idao.TermThemeIDAO;

@Remote(TermThemeIBusiness.class)
@Stateless
public class TermThemeBusiness implements TermThemeIBusiness{

	@EJB
	private TermThemeIDAO dao;
	
	@Override
	public List<TermTheme> findAllThemes() {
		List<TermTheme> themes = dao.getAll();		
		return themes;
	}

}
