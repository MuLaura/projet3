package fr.eql.ai110.projet3.seed.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.ExerciseType;
import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.ibusiness.ExerciseIBusiness;
import fr.eql.ai110.projet3.seed.ibusiness.TermIBusiness;
import fr.eql.ai110.projet3.seed.idao.ExerciseIDAO;
import fr.eql.ai110.projet3.seed.idao.ExerciseTypeIDAO;
import fr.eql.ai110.projet3.seed.idao.LearningIDAO;
import fr.eql.ai110.projet3.seed.idao.TermIDAO;

@Remote(ExerciseIBusiness.class)
@Stateless
public class ExerciseBusiness implements ExerciseIBusiness {

	@EJB
	private TermIBusiness termBus;

	@EJB
	private ExerciseTypeIDAO exTypeDAO;

	@EJB
	private ExerciseIDAO exDAO;

	
	@Override
	public List<Exercise> findExercisesForSessionContent(Student student) {
		List<Exercise> sessionExercises = new ArrayList<Exercise>();
		List<ExerciseType> types = exTypeDAO.getAll();

		List<Term> sessionTerms = termBus.getTermsForSessionContent(student);

		for (Term term : sessionTerms) {
			
			List<Exercise> sessionExercisesByTerm = exDAO.getSessionExercisesByStudentAndTerm(student, term);
			
			if (sessionExercisesByTerm.isEmpty()) {
				for (int i=0; i < types.size(); i++) {
					Exercise ex = new Exercise();

					if (i == 0) {
						ex.setExercise_type(types.get(i));
						ex.setQuestionValue(term.getFrenchTerm());
						ex.setExpectedAnswer(term.getEnglishTerm());
						ex.setTerm(term);
					} else if (i == 1) {
						ex.setExercise_type(types.get(i));
						ex.setQuestionValue(term.getEnglishTerm());
						ex.setExpectedAnswer(term.getFrenchTerm());
						ex.setTerm(term);
					} else if (i == 2) {
						ex.setExercise_type(types.get(i));
						ex.setQuestionValue(term.getAudioLink());
						ex.setExpectedAnswer(term.getEnglishTerm().trim());
						ex.setTerm(term);
					} else {
						ex.setExercise_type(types.get(i));
						ex.setQuestionValue(term.getEnglishTerm());
						ex.setExpectedAnswer(term.getPhonetics());
						ex.setTerm(term);
					}

					term.getExercises().add(ex);
				}
			}

			for (Exercise exercise : term.getExercises()) {
				if(!exercise.getExercise_type().getExerciseType().equals(types.get(3).getExerciseType())) {
					sessionExercises.add(exercise);
				}
			}
		}

		Collections.shuffle(sessionExercises);

		return sessionExercises;
	}


	@Override
	public Exercise updateExerciseStats(Exercise currentExercise) {
		Exercise updatedExercise;

		if (currentExercise.getExerciseId() == null) {
			updatedExercise = exDAO.add(currentExercise);

		} else {	
			updatedExercise = exDAO.update(currentExercise);
		}

		return updatedExercise;
	}



}
