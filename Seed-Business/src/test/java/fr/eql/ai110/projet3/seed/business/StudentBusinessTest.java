package fr.eql.ai110.projet3.seed.business;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StudentBusinessTest {
	
	private StudentBusiness studBus;
	
	@BeforeAll
	public static void SetUp() {
		
	}
	
	@BeforeEach
	public void beforeEach() {
		studBus = new StudentBusiness();
	}
	
	//lower than min length is false
	@Test
	public void UsernameShorterThanMinLength_IsIncorrect() {
		String username = "me";
		assertFalse(studBus.validateUsernameConstraints(username));
	}
	
	//higher than maxlength is false
	@Test
	public void UsernameLongerThanMaxLength_IsIncorrect() {
		String username = "MostNobleBenedictOfBirminghamTheThird";
		assertFalse(studBus.validateUsernameConstraints(username));
	}
	
	//with . is correct
	@Test
	public void UsernameWithFullStop_IsCorrect() {
		String username = "Harry.Potter";
		assertTrue(studBus.validateUsernameConstraints(username));
	}
	
	//with _ is correct
	@Test
	public void UsernameWithUnderscore_IsCorrect() {
		String username = "Gandalf_TheGrey";
		assertTrue(studBus.validateUsernameConstraints(username));
	}
	
	//with - is correct
	@Test
	public void UsernameWithDash_IsCorrect() {
		String username = "Darth-Vador";
		assertTrue(studBus.validateUsernameConstraints(username));
	}
	
	// with # is incorrect
	@Test
	public void UsernameWithHashtag_IsIncorrect() {
		String username = "#needsleep";
		assertFalse(studBus.validateUsernameConstraints(username));
	}
	
	//with " is incorrect
	@Test
	public void UsernameWithBackQuotes_IsIncorrect() {
		String username = "Hey`Jude";
		assertFalse(studBus.validateUsernameConstraints(username));
	}
	
}
