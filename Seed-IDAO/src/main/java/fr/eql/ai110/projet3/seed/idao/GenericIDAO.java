package fr.eql.ai110.projet3.seed.idao;

import java.util.List;

public interface GenericIDAO<T> {
	
	/**
	 * Add an object to the database
	 * @return added object
	 */
	T add(T t);	
	
	/**
	 * Delete an object from the database
	 * @return boolean true if correctly removed
	 */
	boolean delete(T t);
	
	/**
	 * Update the values of an object in the database
	 * @return updated object
	 */
	T update(T t);
	
	/**
	 * Get object from database by ID
	 * @return selected object
	 */
	T getById(int id);
	
	/**
	 * Get all objects in a table
	 * @return result list
	 */
	List<T> getAll();

}
