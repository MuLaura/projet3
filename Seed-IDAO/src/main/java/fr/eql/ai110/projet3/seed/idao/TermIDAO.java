package fr.eql.ai110.projet3.seed.idao;

import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Term;

public interface TermIDAO extends GenericIDAO<Term>{

	/**
	 * Get all terms for a given studentLevel (matches parameter value)
	 * @param lvl
	 * @return list of Terms
	 */
	List<Term> getAllByLevel(int lvl);
	
	/**
	 * Get all terms whose theme matches the parameter value
	 * @param theme
	 * @return List of Term objects
	 */
	List<Term> getAllByTheme(String theme);
	
	/**
	 * Get the number of terms for a given level
	 * @param lvl
	 * @return Long number of terms in level
	 */
	Long findNbTermsByLevel(int lvl);
	
	/**
	 * Get a term whose id matched the parameter value
	 * @param termId
	 * @return Term object
	 */
	Term getTermByID(Integer termId);

	
}
