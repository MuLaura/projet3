package fr.eql.ai110.projet3.seed.idao;

import fr.eql.ai110.projet3.seed.entity.AcquisitionLevel;

public interface AcquisitionLevelIDAO extends GenericIDAO<AcquisitionLevel>{

	/**
	 * Retrieves an acquisitionLevel by it's name
	 * @param level is the String name of the acquistionLevel desired
	 * @returns aquisitionLevel object 
	 */
	AcquisitionLevel getByName(String string);

}
