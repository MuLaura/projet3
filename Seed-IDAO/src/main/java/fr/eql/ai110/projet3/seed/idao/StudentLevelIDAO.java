package fr.eql.ai110.projet3.seed.idao;

import fr.eql.ai110.projet3.seed.entity.StudentLevel;

public interface StudentLevelIDAO extends GenericIDAO<StudentLevel>{

	/**
	 * Get studentLevel where intLevel value matches parameter
	 * @param i
	 * @return
	 */
	StudentLevel getByIntLvl(int i);


}
