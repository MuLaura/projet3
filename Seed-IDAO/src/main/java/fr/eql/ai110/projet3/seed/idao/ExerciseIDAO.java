package fr.eql.ai110.projet3.seed.idao;

import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;

public interface ExerciseIDAO extends GenericIDAO<Exercise> {

	/**
	 * Get an exercise of a specific type for a given term
	 * 
	 * @param termID references the term
	 * @param exerciseType references the exercise type for a given term
	 * @returns single Exercise where termID and exerciseType match the parameters
	 */
	Exercise getExerciseByTermIDAndExType(Integer termId, String exerciseType);

	/**
	 * Get all a student's exercises for a given term
	 * @param student is the connected user
	 * @param term is the term whose exercises need to be retrieved
	 * @returns List of exercises
	 */
	List<Exercise> getSessionExercisesByStudentAndTerm(Student student, Term term);

}
