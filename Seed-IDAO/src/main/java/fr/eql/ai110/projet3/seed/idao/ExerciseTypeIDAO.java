package fr.eql.ai110.projet3.seed.idao;

import fr.eql.ai110.projet3.seed.entity.ExerciseType;

public interface ExerciseTypeIDAO extends GenericIDAO<ExerciseType>{

}
