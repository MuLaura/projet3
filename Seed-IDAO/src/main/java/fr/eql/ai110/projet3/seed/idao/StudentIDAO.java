package fr.eql.ai110.projet3.seed.idao;

import fr.eql.ai110.projet3.seed.entity.Student;

public interface StudentIDAO extends GenericIDAO<Student>{

	/**
	 * Get student from database where login and hashed password equal the parameters 
	 * @param login
	 * @param hashedPassword
	 * @return retrieved student OR null if no match 
	 */
	Student authenticate(String login, String hashedPassword);
	
	/**
	 * Get student from database where username matches parameter value
	 * @param username
	 * @return retrieved student OR null if no match 
	 */
	Student getStudentByUsername(String username);
	
	/**
	 * Get student where Student.id matches parameter value
	 * @param id
	 * @return student
	 */
	Student getStudentByID(int id);
	
	/**
	 * Get student from database where email matches parameter value
	 * @param email
	 * @return retrieved student OR null if no match 
	 */
	Student getStudentByEmail(String email);
	
}
