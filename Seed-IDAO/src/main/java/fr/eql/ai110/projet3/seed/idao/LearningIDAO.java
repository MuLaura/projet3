package fr.eql.ai110.projet3.seed.idao;

import java.time.LocalDate;
import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;

public interface LearningIDAO extends GenericIDAO<Learning>{
	
	/**
	 * Get the number of terms in a level according to their acquisitionLevel and the studentID
	 * @param acqLvl is the acquisitionLevel requested
	 * @param student is the connected user
	 * @return Long number of terms in a given acquisitionLevel for a given user
	 */
	Long getNbTermByAcqLvlAndTermLvl(String acqLvl, Student student);
	
	/**
	 * Get all new lessons available to a connected user based on their current level
	 * @param student is the connected user
	 * @return list of available lessons
	 */
	List<Learning> getNewLessons(Student student);
	
	/**
	 * Get all exercises available to a connected user based on the date of their next review
	 * @param student
	 * @return List of Learning object where the reviewDate is less or equal to the current date
	 */
	List<Learning> getAvailableExercises(Student student);
	
	/**
	 * Get list of learning where the next review date is reviewDate
	 * @param reviewDate
	 * @param student
	 * @return list of upcoming reviews
	 */
	List<Learning> getUpcomingReviews(LocalDate reviewDate, Student student);
	
	/**
	 * Get all Learning for a given student at a given level
	 * @param student
	 * @return list of Learning object
	 */
	List<Learning> getAllLockedByLvlAndStudent(Student student);
	
	/**
	 * For a given student, get the number of all terms in a level minus the ones whose acquisitionLevel 
	 * equals the parameter acqLevel 
	 * 
	 * @param acqLvl is the acquisitionLevel requested
	 * @param student is the connected user
	 * @return Long number of terms in a level for a given user whose acquisitionLevel does not match the parameter
	 */
	Long getNbTermByAcqLvlAndTermLvlForAllButOne(String acqLvl, Student student);
}
