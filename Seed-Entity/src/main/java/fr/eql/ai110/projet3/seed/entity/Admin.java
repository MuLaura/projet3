package fr.eql.ai110.projet3.seed.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="admin")
public class Admin extends User {

	private static final long serialVersionUID = 1L;
	
	@Column(name="admin_code")
	private String adminCode;
	
	public Admin() {
		super();
	}
	
	public Admin(String adminCode) {
		super();
		this.adminCode = adminCode;
	}
	
	public String getAdminCode() {
		return adminCode;
	}
	public void setAdminCode(String adminCode) {
		this.adminCode = adminCode;
	}

	
	
}
