package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="exercise_type")
public class ExerciseType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer exerxiseTypeId;
	@Column(name="exercise_type")
	private String exerciseType;
	@Column(name="instruction", columnDefinition="LONGTEXT")
	private String instruction;
	
	@OneToMany(mappedBy = "exercise_type", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Exercise> exercises;

	public ExerciseType() {
		super();
	}

	public ExerciseType(Integer exerxiseTypeId, String exerciseType, String instruction, Set<Exercise> exercises) {
		super();
		this.exerxiseTypeId = exerxiseTypeId;
		this.exerciseType = exerciseType;
		this.instruction = instruction;
		this.exercises = exercises;
	}



	public Integer getExerxiseTypeId() {
		return exerxiseTypeId;
	}

	public void setExerxiseTypeId(Integer exerxiseTypeId) {
		this.exerxiseTypeId = exerxiseTypeId;
	}

	public String getExerciseType() {
		return exerciseType;
	}

	public void setExerciseType(String exerciseType) {
		this.exerciseType = exerciseType;
	}



	public String getInstruction() {
		return instruction;
	}



	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}



	public Set<Exercise> getExercises() {
		return exercises;
	}



	public void setExercises(Set<Exercise> exercises) {
		this.exercises = exercises;
	}

	
	
}
