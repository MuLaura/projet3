package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="studentlevel")
public class StudentLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer levelId;
	@Column(name = "level")
	private String levelName;
	@Column(name = "intlevel")
	private Integer intLevel;
	
	@OneToMany(mappedBy = "studentlevel", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<Student> students;
	@OneToMany(mappedBy = "studentlevel", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Term> terms;
	
	public StudentLevel() {
		super();
	}
	


	public StudentLevel(Integer levelId, String levelName, Integer intLevel, Set<Student> students, Set<Term> terms) {
		super();
		this.levelId = levelId;
		this.levelName = levelName;
		this.intLevel = intLevel;
		this.students = students;
		this.terms = terms;
	}

	public Integer getLevelId() {
		return levelId;
	}
	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}
	public Set<Student> getStudents() {
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	public Set<Term> getTerms() {
		return terms;
	}
	public void setTerms(Set<Term> terms) {
		this.terms = terms;
	}



	public String getLevelName() {
		return levelName;
	}



	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}



	public Integer getIntLevel() {
		return intLevel;
	}



	public void setIntLevel(Integer intLevel) {
		this.intLevel = intLevel;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
