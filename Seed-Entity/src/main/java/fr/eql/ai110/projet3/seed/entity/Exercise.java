package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "exercise")
public class Exercise implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer exerciseId;
	@Column(name = "question")
	private String questionValue;
	@Column(name = "answer")
	private String expectedAnswer;
	@Column(name="nbcorrect")
	private Integer nbCorrect;
	@Column(name="nbincorrect")
	private Integer nbIncorrect;
	@Column(name="total_answers")
	private Integer totalAnswers;
	@Column(name="streak")
	private Integer exerciseStreak;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "user_id")
	private Student student;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private ExerciseType exercise_type;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Term term;
	
	public Exercise() {
		super();
	}
	
	
	
	public Exercise(Integer exerciseId, String questionValue, String expectedAnswer, Integer nbCorrect,
			Integer nbIncorrect, Integer totalAnswers, Integer exerciseStreak, Student student,
			ExerciseType exercise_type, Term term) {
		super();
		this.exerciseId = exerciseId;
		this.questionValue = questionValue;
		this.expectedAnswer = expectedAnswer;
		this.nbCorrect = nbCorrect;
		this.nbIncorrect = nbIncorrect;
		this.totalAnswers = totalAnswers;
		this.exerciseStreak = exerciseStreak;
		this.student = student;
		this.exercise_type = exercise_type;
		this.term = term;
	}



	public Integer getExerciseId() {
		return exerciseId;
	}
	public void setExerciseId(Integer exerciseId) {
		this.exerciseId = exerciseId;
	}
	public Integer getNbCorrect() {
		return nbCorrect;
	}
	public void setNbCorrect(Integer nbCorrect) {
		this.nbCorrect = nbCorrect;
	}
	public Integer getNbIncorrect() {
		return nbIncorrect;
	}
	public void setNbIncorrect(Integer nbIncorrect) {
		this.nbIncorrect = nbIncorrect;
	}
	public Integer getTotalAnswers() {
		return totalAnswers;
	}
	public void setTotalAnswers(Integer totalAnswers) {
		this.totalAnswers = totalAnswers;
	}
	public Integer getExerciseStreak() {
		return exerciseStreak;
	}
	public void setExerciseStreak(Integer exerciseStreak) {
		this.exerciseStreak = exerciseStreak;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Term getTerm() {
		return term;
	}
	public void setTerm(Term term) {
		this.term = term;
	}

	public String getQuestionValue() {
		return questionValue;
	}

	public void setQuestionValue(String questionValue) {
		this.questionValue = questionValue;
	}

	public String getExpectedAnswer() {
		return expectedAnswer;
	}

	public void setExpectedAnswer(String expectedAnswer) {
		this.expectedAnswer = expectedAnswer;
	}

	public ExerciseType getExercise_type() {
		return exercise_type;
	}

	public void setExercise_type(ExerciseType exercise_type) {
		this.exercise_type = exercise_type;
	}

}
