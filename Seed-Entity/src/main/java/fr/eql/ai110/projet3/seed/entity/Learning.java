package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="learning")
public class Learning implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "learning_id")
	private Integer learningId;
	@Column(name = "unlock_date")
	private LocalDateTime unlockDate;
	@Column(name = "last_view_date")
	private LocalDateTime lastViewDate;
	@Column(name = "next_review_date")
	private LocalDateTime nextReviewDate;
	@Column(name="total_views")
	private Integer totalViews;
	@Column(name="total_correct")
	private Integer totalCorrect;
	@Column(name="revision_streak")
	private Integer revisionStreak;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "user_id")
	private Student student;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private AcquisitionLevel acquisitionlvl;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Term term;
	
	public Learning() {
		super();
	}
	
	public Learning(Integer learningId, LocalDateTime unlockDate, LocalDateTime lastViewDate, LocalDateTime nextReviewDate, Integer totalViews,
			Integer totalCorrect, Integer revisionStreak, Student student, AcquisitionLevel acquisitionLvl, Term term) {
		super();
		this.learningId = learningId;
		this.unlockDate = unlockDate;
		this.lastViewDate = lastViewDate;
		this.nextReviewDate = nextReviewDate;
		this.totalViews = totalViews;
		this.totalCorrect = totalCorrect;
		this.revisionStreak = revisionStreak;
		this.student = student;
		this.acquisitionlvl = acquisitionLvl;
		this.term = term;
	}

	public Integer getLearningId() {
		return learningId;
	}

	public void setLearningId(Integer learningId) {
		this.learningId = learningId;
	}

	public LocalDateTime getUnlockDate() {
		return unlockDate;
	}

	public void setUnlockDate(LocalDateTime unlockDate) {
		this.unlockDate = unlockDate;
	}

	public LocalDateTime getLastViewDate() {
		return lastViewDate;
	}

	public void setLastViewDate(LocalDateTime lastViewDate) {
		this.lastViewDate = lastViewDate;
	}

	public LocalDateTime getNextReviewDate() {
		return nextReviewDate;
	}

	public void setNextReviewDate(LocalDateTime nextReviewDate) {
		this.nextReviewDate = nextReviewDate;
	}

	public Integer getTotalViews() {
		return totalViews;
	}

	public void setTotalViews(Integer totalViews) {
		this.totalViews = totalViews;
	}

	public Integer getTotalCorrect() {
		return totalCorrect;
	}

	public void setTotalCorrect(Integer totalCorrect) {
		this.totalCorrect = totalCorrect;
	}

	public Integer getRevisionStreak() {
		return revisionStreak;
	}

	public void setRevisionStreak(Integer revisionStreak) {
		this.revisionStreak = revisionStreak;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public AcquisitionLevel getAcquisitionlvl() {
		return acquisitionlvl;
	}

	public void setAcquisitionlvl(AcquisitionLevel acquisitionlvl) {
		this.acquisitionlvl = acquisitionlvl;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

}
