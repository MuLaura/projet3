package fr.eql.ai110.projet3.seed.entity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="student")
public class Student extends User {

	private static final long serialVersionUID = 1L; 
	
	@Column(name = "student_lesson_nb")
	private Integer nbLessonsPerClass;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Exercise> exercises;
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Learning> learnings;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private StudentLevel studentlevel;
	
	public Student() {
		super();
	}

	public Student(Integer nbLessonsPerClass, List<Exercise> exercises, List<Learning> learnings,
			StudentLevel level) {
		super();
		this.nbLessonsPerClass = nbLessonsPerClass;
		this.exercises = exercises;
		this.learnings = learnings;
		this.studentlevel = level;
	}
	
	

	public Student(Integer userId, String username, String password, String salt, String email, LocalDate signupDate,
			LocalDate deactivationDate, LocalDate acceptTermsConditions, boolean acceptNotifications, Integer nbLessonsPerClass, List<Exercise> exercises, List<Learning> learnings,
			StudentLevel level) {
		super(userId, username, password, salt, email, signupDate, deactivationDate, acceptTermsConditions,
				acceptNotifications);
		this.nbLessonsPerClass = nbLessonsPerClass;
		this.exercises = exercises;
		this.learnings = learnings;
		this.studentlevel = level;
	}

	public Integer getNbLessonsPerClass() {
		return nbLessonsPerClass;
	}

	public void setNbLessonsPerClass(Integer nbLessonsPerClass) {
		this.nbLessonsPerClass = nbLessonsPerClass;
	}

	public List<Exercise> getExercises() {
		return exercises;
	}

	public void setExercises(List<Exercise> exercises) {
		this.exercises = exercises;
	}

	public List<Learning> getLearnings() {
		return learnings;
	}

	public void setLearnings(List<Learning> learnings) {
		this.learnings = learnings;
	}

	public StudentLevel getStudentlevel() {
		return studentlevel;
	}

	public void setStudentlevel(StudentLevel studentlevel) {
		this.studentlevel = studentlevel;
	}


	
}
