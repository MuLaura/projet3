package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "salt")
	private String salt;
	@Column(name = "email")
	private String email;
	@Column(name = "signup_date")
	private LocalDate signupDate;
	@Column(name = "deactivation_date")
	private LocalDate deactivationDate;
	@Column(name = "accept_terms_date")
	private LocalDate acceptTermsConditions;
	@Column(name = "notifications")
	private boolean acceptNotifications;
	
	public User() {
		super();
	}



	public User(Integer userId, String username, String password, String salt, String email, LocalDate signupDate,
			LocalDate deactivationDate, LocalDate acceptTermsConditions, boolean acceptNotifications) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.salt = salt;
		this.email = email;
		this.signupDate = signupDate;
		this.deactivationDate = deactivationDate;
		this.acceptTermsConditions = acceptTermsConditions;
		this.acceptNotifications = acceptNotifications;
	}



	public void setSignupDate(LocalDate signupDate) {
		this.signupDate = signupDate;
	}



	public void setDeactivationDate(LocalDate deactivationDate) {
		this.deactivationDate = deactivationDate;
	}



	public void setAcceptTermsConditions(LocalDate acceptTermsConditions) {
		this.acceptTermsConditions = acceptTermsConditions;
	}



	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAcceptNotifications() {
		return acceptNotifications;
	}

	public void setAcceptNotifications(boolean acceptNotifications) {
		this.acceptNotifications = acceptNotifications;
	}

	

}
