package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "acquisitionlvl")
public class AcquisitionLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer acquisitionlvlId;
	@Column(name = "lvlname")
	private String lvlName;
	@Column(name = "cooldown_period")
	private long acqLvlIntervalInHours;

	@OneToMany(mappedBy = "acquisitionlvl", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Learning> learnings;

	public AcquisitionLevel() {
		super();
	}

	public AcquisitionLevel(Integer acquisitionlvlId, String lvlName, long acqLvlIntervalInHours,
			Set<Learning> learnings) {
		super();
		this.acquisitionlvlId = acquisitionlvlId;
		this.lvlName = lvlName;
		this.acqLvlIntervalInHours = acqLvlIntervalInHours;
		this.learnings = learnings;
	}

	public Integer getAcquisitionlvlId() {
		return acquisitionlvlId;
	}

	public void setAcquisitionlvlId(Integer acquisitionlvlId) {
		this.acquisitionlvlId = acquisitionlvlId;
	}

	public String getLvlName() {
		return lvlName;
	}

	public void setLvlName(String lvlName) {
		this.lvlName = lvlName;
	}

	public Set<Learning> getLearnings() {
		return learnings;
	}

	public void setLearnings(Set<Learning> learnings) {
		this.learnings = learnings;
	}

	public long getAcqLvlIntervalInHours() {
		return acqLvlIntervalInHours;
	}

	public void setAcqLvlIntervalInHours(long acqLvlIntervalInHours) {
		this.acqLvlIntervalInHours = acqLvlIntervalInHours;
	}



	
	
}
