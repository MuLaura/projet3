package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="term_theme")
public class TermTheme implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer termThemeId;
	@Column(name="theme_name")
	private String theme;
	
	@OneToMany(mappedBy = "theme", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Term> terms;
	
	public TermTheme() {
		super();
	}

	public TermTheme(Integer termThemeId, String theme, Set<Term> terms) {
		super();
		this.termThemeId = termThemeId;
		this.theme = theme;
		this.terms = terms;
	}

	public Integer getTermThemeId() {
		return termThemeId;
	}

	public void setTermThemeId(Integer termThemeId) {
		this.termThemeId = termThemeId;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Set<Term> getTerms() {
		return terms;
	}

	public void setTerms(Set<Term> terms) {
		this.terms = terms;
	}

}
