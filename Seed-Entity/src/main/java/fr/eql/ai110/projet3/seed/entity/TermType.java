package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="term_type")
public class TermType implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer termTypeId;
	@Column(name="term_type")
	private String termType;
	
	@OneToMany(mappedBy = "type", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Term> terms;

	public TermType(Integer termTypeId, String termType, Set<Term> terms) {
		super();
		this.termTypeId = termTypeId;
		this.termType = termType;
		this.terms = terms;
	}

	public TermType() {
		super();
	}

	public Integer getTermTypeId() {
		return termTypeId;
	}

	public void setTermTypeId(Integer termTypeId) {
		this.termTypeId = termTypeId;
	}

	public String getTermType() {
		return termType;
	}

	public void setTermType(String termType) {
		this.termType = termType;
	}

	public Set<Term> getTerms() {
		return terms;
	}

	public void setTerms(Set<Term> terms) {
		this.terms = terms;
	}
	
	
}
