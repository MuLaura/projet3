package fr.eql.ai110.projet3.seed.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="term")
public class Term implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer termId;
	@Column(name = "english")
	private String englishTerm;
	@Column(name = "french")
	private String frenchTerm;
	@Column(name = "definition", columnDefinition="LONGTEXT")
	private String definition;
	@Column(name = "phonetics")
	private String phonetics;
	@Column(name = "audio")
	private String audioLink;
	@Column(name = "mnemonics", columnDefinition="LONGTEXT")
	private String mnemonics;

	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private StudentLevel studentlevel;
	@OneToMany(mappedBy = "term", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<Exercise> exercises;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private TermTheme theme;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private TermType type;
	@OneToMany(mappedBy = "term", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Learning> learnings;
	
	public Term() {
		super();
	}

	public Term(Integer termId, String englishTerm, String frenchTerm, String definition, String phonetics,
			String audioLink, String mnemonics, StudentLevel studentlevel, Set<Exercise> exercises, TermTheme theme,
			TermType type, Set<Learning> learnings) {
		super();
		this.termId = termId;
		this.englishTerm = englishTerm;
		this.frenchTerm = frenchTerm;
		this.definition = definition;
		this.phonetics = phonetics;
		this.audioLink = audioLink;
		this.mnemonics = mnemonics;
		this.studentlevel = studentlevel;
		this.exercises = exercises;
		this.theme = theme;
		this.type = type;
		this.learnings = learnings;
	}



	public Integer getTermId() {
		return termId;
	}
	public void setTermId(Integer termId) {
		this.termId = termId;
	}
	public String getEnglishTerm() {
		return englishTerm;
	}
	public void setEnglishTerm(String englishTerm) {
		this.englishTerm = englishTerm;
	}
	public String getFrenchTerm() {
		return frenchTerm;
	}
	public void setFrenchTerm(String frenchTerm) {
		this.frenchTerm = frenchTerm;
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public String getPhonetics() {
		return phonetics;
	}
	public void setPhonetics(String phonetics) {
		this.phonetics = phonetics;
	}
	public String getAudioLink() {
		return audioLink;
	}
	public void setAudioLink(String audioLink) {
		this.audioLink = audioLink;
	}
	public Set<Exercise> getExercises() {
		return exercises;
	}
	public void setExercises(Set<Exercise> exercises) {
		this.exercises = exercises;
	}
	
	public TermTheme getTheme() {
		return theme;
	}

	public void setTheme(TermTheme theme) {
		this.theme = theme;
	}

	public TermType getType() {
		return type;
	}
	public void setType(TermType type) {
		this.type = type;
	}

	public String getMnemonics() {
		return mnemonics;
	}

	public void setMnemonics(String mnemonics) {
		this.mnemonics = mnemonics;
	}

	public StudentLevel getStudentlevel() {
		return studentlevel;
	}

	public void setStudentlevel(StudentLevel studentlevel) {
		this.studentlevel = studentlevel;
	}

	public Set<Learning> getLearnings() {
		return learnings;
	}

	public void setLearnings(Set<Learning> learnings) {
		this.learnings = learnings;
	}
	
}
