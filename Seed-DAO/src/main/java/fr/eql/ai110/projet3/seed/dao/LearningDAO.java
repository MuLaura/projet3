package fr.eql.ai110.projet3.seed.dao;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.idao.LearningIDAO;

/**
 * @author formation
 *
 */
/**
 * @author formation
 *
 */
@Remote(LearningIDAO.class)
@Stateless
public class LearningDAO extends GenericDAO<Learning> implements LearningIDAO {

	@Override
	public Long getNbTermByAcqLvlAndTermLvl(String acqLvl, Student student) {
		Query query = em.createQuery("SELECT COUNT(l) FROM Learning l "
				+ "JOIN l.acquisitionlvl a "
				+ "JOIN l.student s "
				+ "JOIN l.term t "
				+ "JOIN t.studentlevel sl "
				+ "WHERE a.lvlName LIKE CONCAT('%', :acqParam, '%') "
				+ "AND s.userId = :studentIdParam "
				+ "AND sl.intLevel = :lvlParam ");
		query.setParameter("acqParam", acqLvl);
		query.setParameter("studentIdParam", student.getUserId());
		query.setParameter("lvlParam", student.getStudentlevel().getIntLevel());
		
		return (Long) query.getSingleResult();
	}

	
	@Override
	public List<Learning> getUpcomingReviews(LocalDate reviewDate, Student student) {
		Date sqlDate = Date.valueOf(reviewDate);
		
		Query query = em.createQuery("SELECT l FROM Learning l "
				+ "JOIN l.student s "
				+ "WHERE DATE(l.nextReviewDate) = :dateParam AND s.userId = :studentIdParam");
		query.setParameter("dateParam", sqlDate);
		query.setParameter("studentIdParam", student.getUserId());
		List<Learning> nextReviews = query.getResultList();
		
		return nextReviews;
	}
	
	
	@Override
	public List<Learning> getNewLessons(Student student) {
		
		Date today = Date.valueOf(LocalDate.now());
		Query query = em.createQuery("SELECT l FROM Learning l "
				+ "JOIN l.student s "
				+ "JOIN l.term t "
				+ "JOIN t.studentlevel sl "
				+ "JOIN t.theme th "
				+ "JOIN t.type ty "
				+ "WHERE s.userId = :studentID "
				+ "AND DATE(l.unlockDate) <= :unlockDate "
				+ "AND DATE(l.unlockDate) IS NOT NULL "
				+ "AND DATE(l.lastViewDate) IS NULL", Learning.class);
		
		query.setParameter("unlockDate", today);
		query.setParameter("studentID", student.getUserId());
	
		return query.getResultList();
	}

	@Override
	public List<Learning> getAvailableExercises(Student student) {
		Date today = Date.valueOf(LocalDate.now());
		Query query = em.createQuery("SELECT l FROM Learning l "
				+ "JOIN l.student s "
				+ "JOIN l.acquisitionlvl a "
				+ "WHERE s.userId = :studentID "
				+ "AND DATE(l.unlockDate) <= :currentDate "
				+ "AND DATE(l.unlockDate) IS NOT NULL "
				+ "AND l.nextReviewDate <= CURRENT_TIMESTAMP "
				+ "AND a.lvlName <> :lvlParam ", Learning.class);
		
		query.setParameter("studentID", student.getUserId());
		query.setParameter("currentDate", today);
		query.setParameter("lvlParam", "Grand Sage");
		
		return query.getResultList();
	}

	@Override
	public List<Learning> getAllLockedByLvlAndStudent(Student student) {
		Query query = em.createQuery("SELECT l FROM Learning l "
				+ "JOIN l.student s "
				+ "JOIN t.studentlevel sl "
				+ "JOIN l.term t "	
				+ "WHERE s.userId = :studentID "
				+ "AND sl.intLevel = :level ", Learning.class);
		
		query.setParameter("studentID", student.getUserId());
		query.setParameter("level", student.getStudentlevel().getIntLevel());
		
		return query.getResultList();
	}

	@Override
	public Long getNbTermByAcqLvlAndTermLvlForAllButOne(String acqLvl, Student student) {
		Query query = em.createQuery("SELECT COUNT(l) FROM Learning l "
				+ "JOIN l.acquisitionlvl a "
				+ "JOIN l.student s "
				+ "JOIN l.term t "
				+ "JOIN t.studentlevel sl "
				+ "WHERE a.lvlName NOT LIKE CONCAT('%', :acqParam, '%') "
				+ "AND s.userId = :studentIdParam "
				+ "AND sl.intLevel = :lvlParam ");
		query.setParameter("acqParam", acqLvl);
		query.setParameter("studentIdParam", student.getUserId());
		query.setParameter("lvlParam", student.getStudentlevel().getIntLevel());
		
		return (Long) query.getSingleResult();
	}
	
}


