package fr.eql.ai110.projet3.seed.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.AcquisitionLevel;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.idao.AcquisitionLevelIDAO;

@Remote(AcquisitionLevelIDAO.class)
@Stateless
public class AcquisitionLevelDAO extends GenericDAO<AcquisitionLevel> implements AcquisitionLevelIDAO {

	@Override
	public AcquisitionLevel getByName(String level) {
		AcquisitionLevel lvl = new AcquisitionLevel();
		Query query = em.createQuery("SELECT a FROM AcquisitionLevel a "
				+ "WHERE a.lvlName = :lvlParam");
		query.setParameter("lvlParam", level);
		lvl = (AcquisitionLevel) query.getSingleResult();
		return lvl; 
	}

}
