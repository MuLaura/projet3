package fr.eql.ai110.projet3.seed.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.idao.StudentLevelIDAO;

@Remote(StudentLevelIDAO.class)
@Stateless
public class StudentLevelDAO extends GenericDAO<StudentLevel> implements StudentLevelIDAO {

	@Override
	public StudentLevel getByIntLvl(int intLvl) {
		StudentLevel lvl = new StudentLevel();
		Query query = em.createQuery("SELECT s FROM StudentLevel s "
				+ "WHERE s.intLevel = :lvlParam");
		query.setParameter("lvlParam", intLvl);
		lvl = (StudentLevel) query.getSingleResult();
		return lvl; 
	}

	
	
}
