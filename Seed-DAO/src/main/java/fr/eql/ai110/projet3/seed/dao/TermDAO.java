package fr.eql.ai110.projet3.seed.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.idao.TermIDAO;

@Remote(TermIDAO.class)
@Stateless
public class TermDAO extends GenericDAO<Term> implements TermIDAO {

	@Override
	public List<Term> getAllByLevel(int lvl) {
		
		Query query = em.createQuery("SELECT t FROM Term t "
				+ "JOIN t.studentlevel s "
				+ "JOIN t.theme th "
				+ "JOIN t.type ty "
				+ "WHERE s.intLevel = :lvlParam", Term.class);
		query.setParameter("lvlParam", lvl);
		List<Term> terms = query.getResultList();
		
		return terms;
	}

	@Override
	public List<Term> getAllByTheme(String theme) {
		Query query = em.createQuery("SELECT t FROM Term t "
				+ "JOIN t.studentlevel s "
				+ "JOIN t.theme th "
				+ "JOIN t.type ty "
				+ "WHERE th.theme = :themeParam", Term.class);
		query.setParameter("themeParam", theme);
		List<Term> terms = query.getResultList();
		
		return terms;
	}

	@Override
	public Long findNbTermsByLevel(int lvl) {
			Query query = em.createQuery("SELECT COUNT(t) FROM Term t "
					+ "JOIN t.studentlevel s "
					+ "WHERE s.intLevel = :lvlParam");
			query.setParameter("lvlParam", lvl);
			return (Long) query.getSingleResult();
	}

	@Override
	public Term getTermByID(Integer termId) {
		Term term = null;
		
		Query query = em.createQuery("SELECT t FROM Term t WHERE t.id = :termId");
		query.setParameter("termId", termId);
		term = (Term) query.getSingleResult();
		return term;
	}

}
