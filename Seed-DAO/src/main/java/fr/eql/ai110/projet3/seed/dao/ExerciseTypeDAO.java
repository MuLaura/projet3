package fr.eql.ai110.projet3.seed.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.ExerciseType;
import fr.eql.ai110.projet3.seed.idao.ExerciseTypeIDAO;

@Remote(ExerciseTypeIDAO.class)
@Stateless
public class ExerciseTypeDAO extends GenericDAO<ExerciseType> implements ExerciseTypeIDAO {

}
