package fr.eql.ai110.projet3.seed.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.idao.StudentIDAO;

@Remote(StudentIDAO.class)
@Stateless
public class StudentDAO extends GenericDAO<Student> implements StudentIDAO {

	@Override
	public Student getStudentByID(int id) {
		Student student = null;
		
		Query query = em.createQuery("SELECT s FROM Student s WHERE s.user_id = :studentId");
		query.setParameter("studentId", id);
		student = (Student) query.getSingleResult();
		return student;
	}

	@Override
	public Student authenticate(String login, String hashedPassword) {
		Student student = null;
		List<Student> students;
		
		Query query = em.createQuery("SELECT s FROM Student s WHERE s.username = :loginParam AND s.password=:hashedPasswordParam");
		query.setParameter("loginParam", login);
		query.setParameter("hashedPasswordParam", hashedPassword);
		students = query.getResultList();
		if (students.size() > 0) {
			student = students.get(0);
		}	
		return student;
	}

	@Override
	public Student getStudentByUsername(String username) {
		Student student = null;
		List<Student> students;
		
		Query query = em.createQuery("SELECT s FROM Student s WHERE s.username = :loginParam");
		query.setParameter("loginParam", username);
		students = query.getResultList();
		if (students.size() > 0) {
			student = students.get(0);
		}	
		return student;
	}

	@Override
	public Student getStudentByEmail(String email) {
		Student student = null;
		List<Student> students;
		
		Query query = em.createQuery("SELECT s FROM Student s WHERE s.email = :emailParam");
		query.setParameter("emailParam", email);
		students = query.getResultList();
		if (students.size() > 0) {
			student = students.get(0);
		}	
		return student;
	}

}
