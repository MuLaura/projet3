package fr.eql.ai110.projet3.seed.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;
import fr.eql.ai110.projet3.seed.idao.ExerciseIDAO;

@Remote(ExerciseIDAO.class)
@Stateless
public class ExerciseDAO extends GenericDAO<Exercise> implements ExerciseIDAO {
	
	@Override
	public Exercise getExerciseByTermIDAndExType(Integer termID, String exerciseType) {
		Exercise exercise  = null;
		
		Query query = em.createQuery("SELECT e FROM Exercise e "
				+ "JOIN e.term t "
				+ "JOIN e.exerciseType et "
				+ "JOIN e.student s "
				+ "WHERE t.id = :termID "
				+ "AND et.exerciseType = :typeParam ");
		query.setParameter("termID", termID);
		query.setParameter("typeParam", exerciseType);
		
		exercise = (Exercise) query.getSingleResult();
		
		return exercise;
	}


	@Override
	public List<Exercise> getSessionExercisesByStudentAndTerm(Student student, Term term) {
		Query query = em.createQuery("SELECT e FROM Exercise e "
				+ "JOIN e.student s "
				+ "JOIN e.term t "	
				+ "WHERE e.student = :studentParam "
				+ "AND e.term = :termParam ");
		
		query.setParameter("studentParam", student);
		query.setParameter("termParam", term);
		
		return query.getResultList();
	}

}
