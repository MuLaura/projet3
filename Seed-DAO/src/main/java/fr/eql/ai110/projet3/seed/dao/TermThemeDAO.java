package fr.eql.ai110.projet3.seed.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.projet3.seed.entity.TermTheme;
import fr.eql.ai110.projet3.seed.idao.TermThemeIDAO;

@Remote(TermThemeIDAO.class)
@Stateless
public class TermThemeDAO extends GenericDAO<TermTheme> implements TermThemeIDAO {

}
