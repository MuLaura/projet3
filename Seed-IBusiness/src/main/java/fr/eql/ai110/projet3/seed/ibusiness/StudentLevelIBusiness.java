package fr.eql.ai110.projet3.seed.ibusiness;

import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.StudentLevel;

public interface StudentLevelIBusiness {
	
	/**
	 * Find list of levels for a given student
	 * @param student
	 * @return list of ArrayList for : open levels and locked levels
	 */
	List<ArrayList<StudentLevel>> findAllOpenLevels(Student student);
	
}
