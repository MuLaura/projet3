package fr.eql.ai110.projet3.seed.ibusiness;

import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;

public interface TermIBusiness {
	
	/**
	 * Get all terms for a given studentLevel (matches parameter value)
	 * @param lvl
	 * @return list of Term objects
	 */
	List<Term> findAllByLvl(int lvl);
	
	/**
	 * Get all terms whose theme matches the parameter value
	 * @param theme
	 * @return list of Term objects
	 */
	List<Term> findAllByTheme(String theme);
	
	/**
	 * Get the number of terms for a given level
	 * @param lvl
	 * @return int number of terms in level
	 */
	int findNbTermByLvl(int lvl);
	
	/**
	 * Find all terms where the Learning next review date is less or equal to the current date
	 * @param student
	 * @return List of terms
	 */
	List<Term> getTermsForSessionContent(Student student);
}
