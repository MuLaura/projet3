package fr.eql.ai110.projet3.seed.ibusiness;

import java.util.List;

import fr.eql.ai110.projet3.seed.entity.TermTheme;

public interface TermThemeIBusiness {
	
	/**
	 * Find all TermThemes
	 * @return List of TermThemes
	 */
	List<TermTheme> findAllThemes();
	
}
