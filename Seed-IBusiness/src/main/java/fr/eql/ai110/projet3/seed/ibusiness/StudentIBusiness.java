package fr.eql.ai110.projet3.seed.ibusiness;

import fr.eql.ai110.projet3.seed.entity.Student;

public interface StudentIBusiness {
	
	/**
	 * Find student by ID
	 * @param id
	 * @return Student object
	 */
	Student findStudentByID(int id);
	
	/**
	 * Find student whose username and hashed password match the username parameter 
	 * and the password parameter after salt and hash
	 * @param username
	 * @param password
	 * @return Student object
	 */
	Student connect(String username, String password);
	
	/**
	 * Increases student level if not at max level and unlock new level term content
	 * @param student
	 * @return student object with updated level
	 */
	Student levelUp(Student student);

	/**
	 * Verify whether username parameter or email parameter match an existing user
	 * @param username
	 * @param email
	 * @return true if a student object is found
	 */
	boolean verifyIfExists(String username, String email);

	/**
	 * Create new Student, set default values, generate salt and hash the password
	 * @param username
	 * @param password
	 * @param email
	 * @return new Student object
	 */
	Student createNewUser(String username, String password, String email);

	/**
	 * Update student object in database after user modifies some properties
	 * @param student
	 */
	void save(Student student);

}
