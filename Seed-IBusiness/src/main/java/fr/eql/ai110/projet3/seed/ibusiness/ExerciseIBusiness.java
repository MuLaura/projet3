package fr.eql.ai110.projet3.seed.ibusiness;

import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Exercise;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;

public interface ExerciseIBusiness {
	/**
	 * Get all student exercises for each term in the list of terms available for the session and shuffle order. 
	 * @param student
	 * @return List of exercises to be used in the session
	 */
	List<Exercise> findExercisesForSessionContent(Student student);

	/**
	 * Update exercise if exists or add to table if it doesn't
	 * @param currentExercise
	 * @return updated Exercise object
	 */
	Exercise updateExerciseStats(Exercise currentExercise);
	
}
