package fr.eql.ai110.projet3.seed.ibusiness;

import java.time.LocalDate;
import java.util.List;

import fr.eql.ai110.projet3.seed.entity.Learning;
import fr.eql.ai110.projet3.seed.entity.Student;
import fr.eql.ai110.projet3.seed.entity.Term;

public interface LearningIBusiness {
	/**
	 * Get the number of terms in a level according to their acquisitionLevel and the studentID
	 * @param acqLvl is the acquisitionLevel
	 * @param student is the connected user
	 * @return int number of terms in a given acquisitionLevel for a given user (if null returns 0)
	 */
	int findNbByAcqLvlAndStudentLvl(String acqLvl, Student student);
	
	/**
	 * Find list of learning where the next review date is reviewDate
	 * @param reviewDate
	 * @param student
	 * @return list of Learning objects
	 */
	List<Learning> findUpcomingReviews(LocalDate reviewDate, Student student);
	
	/**
	 * Find the number of all new lessons available to a connected user based on their current level
	 * @param student
	 * @return int number of available lessons 
	 */
	int findNbNewLessons(Student student);
	
	/**
	 *  Find the number of all new exercises available to a connected user based on their current level
	 * @param student
	 * @return int number of available exercises
	 */
	int findNbNewExercises(Student student);
	
	/**
	 * Find which exercises are available to a connected user based on the date of their next review 
	 * in the associated Learning object
	 * @param student
	 * @return List of Learning object where the reviewDate is less or equal to the current date
	 */
	List<Learning> findLearningForNewExercises(Student student);
	
	/**
	 * Find all new lessons available to a connected user based on their current level
	 * and restrict the size of the list to the user's preferred number of lessons per session
	 * @param student
	 * @return List of Learning object 
	 */
	List<Learning> findLessonSessionContent(Student student);
	
	/**
	 * Update Learning object values (lastViewDate and NextReviewDate) following lesson session
	 * @param lessonContents
	 * @return true if updated correctly
	 */
	Boolean updateAfterSession(List<Learning> lessonContents);
	
	/**
	 * Update Learning object values based on results of exercise session 
	 * (acquisitionLevel, revisionStreak, totalCorrect, totalViews...)
	 * @param student
	 * @param completedExercises is the list of Learning objects to be updated
	 * @return true if updated correctly
	 */
	Boolean updateAfterExerciseSession(Student student, List<Learning> completedExercises);
	
	/**
	 * For a given student, find the number of all terms in a level minus the ones whose acquisitionLevel
	 * equals the parameter acqLevel 
	 * @param acqLvl1
	 * @param student
	 * @return int number of terms in a level for a given user whose acquisitionLevel does not match the parameter
	 */
	int findNbForAllButOneAcqLvlByAcqLvlAndStudentLvl(String acqLvl1, Student student);
	
}
